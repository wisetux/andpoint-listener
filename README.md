# Andpoint Listener

A simple flask application to handle POST requests. It can receive JSON data on a POST request and display the output on a webpage. 

## Installation

First, you need to clone this repository:

```bash
$ git clone git@gitlab.com:wisetux/andpoint-listener.git
```

Or:

```bash
$ git clone https://gitlab.com/wisetux/andpoint-listener.git
```

Then change into the `andpoint-listener` folder:

```bash
$ cd andpoint-listener
```

Now, we will need to create a virtual environment and install the required dependencies:

```bash
$ python3 -m venv andpoint_venv
$ source andpoint_venv/bin/activate
$ pip3 install flask
```
## Running

**Before running, make sure you have activated the virtual environment.**

Navigate to `andpoint-listener` the folder and start the application:

```bash
$ cd andpoint-listener
$ source andpoint_venv/bin/activate
(andpoint_venv)$ flask run
```

The application will start and will be accessible by navigating to http://localhost:5000.

## Usage
You can now POST JSON data to `http://localhost:5000/events` and the received data can be seen by navigating to http://localhost:5000 in a web browser. Output is displayed in the following format `TIMESTAMP: JSON_DATA`

### Testing
You can also test by running this simple Python code to send data to our application

```python
import requests
payload = [{"label":"one","value":1},{"label":"two","value":2},{"label":"three","value":3}]
r = requests.post("http://127.0.0.1:5000/events", headers={'Content-Type': 'application/json'}, json=payload)
print(r.text)
```
If the data is received successfully, you will get a `200` status and see it print an output `Payload Received!`. And if the data being sent is not JSON formatted, you would be getting a `405` status code with a message that reads `Invalid payload. Not valid JSON data.`
