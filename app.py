import datetime, json
from flask import Flask, request, render_template, Response
app = Flask(__name__)
cowsay="&nbsp;----------------------------\n<&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No data to display&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>\n&nbsp;----------------------------\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\&nbsp;&nbsp;^__^\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\&nbsp;(oo)\________\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(__)\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)\//\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||----W&nbsp;|\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||\n"
@app.route('/')
def index():
    f = open("data.txt", "r")
    if f.mode == 'r':
        data=f.read()
    return render_template('index.html', output=data.replace("\n", "<br>"))
@app.route('/clear_logs')
def clearLog():
    f = open("data.txt", "w")
    f.write(cowsay)
    f.close()
    return 'Logs cleared'
@app.route('/events', methods=['POST'])
def endpoint():
    if request.is_json:
        content = request.get_json()
        f = open("data.txt", "r")
        if f.read() == cowsay:
            f = open("data.txt", "w")
        else:
            f = open("data.txt", "a")
        current_date = datetime.datetime.now()
        f.write(str(current_date) + ": " + str(json.dumps(content)) + "\n")
        f.close()
        return 'Payload Received!'
    else:
        return Response("Invalid payload. Not valid JSON data.", status=405, mimetype='application/json')
if __name__ == '__main__':
    app.run(debug=False, host="0.0.0.0")
